<?php
/**
 * Common functions for Drupal development
 * 
 * @copyright GPL (http://www.gnu.org/copyleft/gpl.html)
 * @author Ken De Volder
 */

/**
 * Display code in an HTML environment
 * Usage example: pre(get_defined_vars());
 * 
 * @param mixed Code or content to display
 * @param boolean Will return code if TRUE or print if FALSE
 */
function pre($vars, $return = false)
{
	$output = '<PRE style="text-align:left">'.(print_r($vars,true)).'</PRE>';
	
	if (!$return)
	{
		echo $output;
	}
	else return $output;
}

/**
 * Drupal seems to cache menu's even though caching is disabled.
 * This function tries to clear every cached element.
 */
function clear_cache()
{
	// Clear caches
	cache_clear_all();
	menu_cache_clear_all();
	module_rebuild_cache();
	
	db_query("DELETE FROM {cache} WHERE 1");
	db_query("DELETE FROM {cache_filter} WHERE 1");
	db_query("DELETE FROM {cache_menu} WHERE 1");
	db_query("DELETE FROM {cache_page} WHERE 1");
}