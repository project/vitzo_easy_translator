<?php
/**
 * XAJAX functions for the translation module
 * 
 * @copyright GPL (http://www.gnu.org/copyleft/gpl.html)
 * @author Ken De Volder & Dmitriy Zavalkin for Vitzo Limited (www.vitzo.com)
 */

/**
 * Delete a translation and reflect this on the page if successful.
 * 
 * @param string ID of the translation to delete
 * @param string Language code - use % as wildcard
 */
function deleteTranslation($lid, $lang)
{
    $xajax = new xajaxResponse();
    
    logTranslateChanges ($lid, $lang, 'delete_translation');
    if (db_query("DELETE FROM {locales_target} WHERE lid = $lid AND language = '$lang'"))
    {
        $xajax->script("$('#$lid').replaceWith('".t("Deleted")."');");
    }
    
    return $xajax;
}

/**
 * Change fuzzy value of a translation and reflect this change on the page if successful.
 * You must define FULL_PATH for this function to locate the following images:
 * 
 *  /img/fuzzy0.png if fuzzy is disabled
 *  /img/fuzzy1.png if fuzzy is enabled
 * 
 * @param string ID of the translation to delete
 * @param string Language code - use % as wildcard
 */
function changeFuzzySetting($lid, $lang)
{
    $lid = mysql_escape_string($lid);
    $lang = mysql_escape_string($lang);
    
    $xajax = new xajaxResponse();
    
    // check is translation already exists
    $already_exists = db_fetch_var("SELECT language FROM {locales_target} WHERE lid = $lid AND language = '$lang'");

    if ($already_exists) {
        $fuzzy = db_fetch_var("SELECT fuzzy FROM {locales_target} WHERE lid = $lid AND language = '$lang'");
        $value = ($fuzzy == 0) ? 1 : 0; // Invert the boolean value
        $image = $value;                // Select the correct image
        
        $query = "UPDATE {locales_target} SET fuzzy = $value WHERE lid = $lid AND language = '$lang'";
    }
    else {
        $value = 1;
        $image = $value;  // Select the correct image
    
        logTranslateChanges($lid, $lang, 'add_translation');
        $query = "INSERT INTO {locales_target} SET lid = $lid, language = '$lang', fuzzy = $value";
    }
    logTranslateChanges($lid, $lang, 'change_fuzzy');

    if (db_query($query))
    {
        $uri = FULL_PATH."/img/fuzzy$image.png";
        $xajax->script("document.getElementById('fuzzy_$lid').src = '$uri';");
    }

    return $xajax;
}

/**
 * Save translation and enables and highlights textarea if successful.
 * If unsuccessful - shows alert message with db error code.
 * 
 * @param string  ID of the translation to save
 * @param string  Language code - use % as wildcard
 * @param string  Translation text
 */
function saveTranslation($lid, $lang, $translation)
{
    $lid = mysql_escape_string($lid);
    $lang = mysql_escape_string($lang);
    $translation = mysql_escape_string(htmlspecialchars($translation));
    
    $xajax = new xajaxResponse();
    
    // check is translation already exists
    $query_check = "SELECT language, translation FROM {locales_target} WHERE lid = $lid AND language = '$lang'";
    $result_check = db_query($query_check);
    $row = db_fetch_array($result_check);

    if ($row["language"]) {
        if ($row["translation"] != $translation) {
            logTranslateChanges($lid, $lang, 'update_translation');
            $query = "UPDATE {locales_target} SET translation = '$translation' WHERE lid = $lid AND language = '$lang'";
            $result = db_query($query);
        } else $result = 1;
    }
    else {
        logTranslateChanges($lid, $lang, 'add_translation');
        $query = "INSERT INTO {locales_target} SET lid = $lid, language = '$lang', translation = '$translation'";
        $result = db_query($query);
    }

    if ($result) $xajax->script("$('#$lid').removeAttr('disabled');$('#$lid').highlightFade();$('#prev_$lid').val($('#$lid').val());");
    else $xajax->alert("DB could not be updated!\r\n".db_error());

    return $xajax;
}

/**
 * Save translation and enables and highlights textarea if successful.
 * If unsuccessful - shows alert message with db error code.
 * 
 * @param string  ID of the translation to save
 * @param string  Language code - use % as wildcard
 * @param string  Translation text
 */
function saveComments($lid, $comments)
{
    $lid = mysql_escape_string($lid);
    $comments = mysql_escape_string(htmlspecialchars($comments));
    
    logTranslateChanges($lid, $lang, 'update_comments');
    $query = "UPDATE {locales_source} SET comments = '$comments' WHERE lid = $lid";

    $xajax = new xajaxResponse();
    
    if (db_query($query)) $xajax->script("$('#$lid').removeAttr('disabled');$('#$lid').highlightFade();$('#prev_$lid').val($('#$lid').val());");
    else $xajax->alert("DB could not be updated!\r\n".db_error());

    return $xajax;
}

/**
 * Log all changes for future rollback feature
 * 
 * @param string  ID of the translation to save
 * @param string  Language code
 * @param string  Action
 */
function logTranslateChanges ($lid, $lang, $action) {
    global $user;
    $uid = $user->uid;
    
    switch($action) {
        case 'add_translation':
            $query_log = "INSERT INTO {translator_log} VALUES('', $lid, '$lang', $uid, '$action', '', ".time().")";
            db_query($query_log);
            break;
        case 'update_translation':
        case 'delete_translation':
            $old_value = mysql_escape_string(db_fetch_var("SELECT translation FROM {locales_target} WHERE lid = $lid AND language = '$lang'"));
            $query_log = "INSERT INTO {translator_log} VALUES('', $lid, '$lang', $uid, '$action', '$old_value', ".time().")";
            db_query($query_log);
            break;
        case 'change_fuzzy':
            $old_value = mysql_escape_string(db_fetch_var("SELECT fuzzy FROM {locales_target} WHERE lid = $lid AND language = '$lang'"));
            $query_log = "INSERT INTO {translator_log} VALUES('', $lid, '$lang', $uid, '$action', '$old_value', ".time().")";
            db_query($query_log);
            break;
        case 'update_comments':
            $old_value = mysql_escape_string(db_fetch_var("SELECT comments FROM {locales_source}} WHERE lid = $lid"));
            $query_log = "INSERT INTO {translator_log} VALUES('', $lid, 'source', $uid, '$action', '$old_value', ".time().")";
            db_query($query_log);
            break;
    }
}