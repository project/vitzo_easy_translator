<?php  
/**
 * Install the Translator module
 * This module relies on te locale tables
 *  
 * @author Ken De Volder & Dmitriy Zavalkin for Vitzo Limited (www.vitzo.com)
 */

/**
 * Implementation of hook_install().
 */
function translator_install() {
    drupal_install_schema('translator');
    
    $ret = array();
    translator_schema_alter($schema);
    foreach ($schema as $table => $fields) {
        foreach ($fields['fields'] as $name => $spec) {
            db_add_field($ret, $table, $name, $spec);
        }
    }

    drupal_set_message("Translator have been installed");
}

/**
 * Implementation of hook_uninstall().
 */
function translator_uninstall() {
    drupal_uninstall_schema('translator');
    
    $ret = array();
    translator_schema_alter($schema);
    foreach ($schema as $table => $fields) {
        foreach ($fields['fields'] as $name => $spec) {
            db_drop_field($ret, $table, $name);
        }
    }
}

/**
 * Implementation of hook_schema().
 */
function translator_schema() {

    // In 'translator_log' table we will log all changes
    $schema['translator_log'] = array(
        'fields' => array(
             'id' => array('type' => 'serial', 'not null' => TRUE, 'disp-width' => '11'),
             'lid' => array('type' => 'int', 'not null' => TRUE, 'disp-width' => '11'),
             'language' => array('type' => 'varchar', 'length' => '12', 'not null' => TRUE),
             'uid' => array('type' => 'int', 'not null' => TRUE, 'disp-width' => '11'),
             'action' => array('type' => 'varchar', 'length' => '255', 'not null' => TRUE),
             'old_value' => array('type' => 'blob', 'not null' => TRUE),
             'time' => array('type' => 'int', 'not null' => TRUE, 'disp-width' => '11')),
        'primary key' => array('id'),
    );

    return $schema;
}

/**
 * Implementation of hook_schema_alter().
 */
function translator_schema_alter(&$schema) {

    // Add Additional fields in 'locales_source' and 'locales_target' tables
    $schema['locales_source']['fields']['comments'] = array(
        'type' => 'varchar', 
        'length' => '255', 
        'not null' => TRUE, 
        'default' => '',
        'description' => t('Comments are only visible to translators. It can be used to specify a context or interpretation which might be useful when translating.')
    );
    $schema['locales_target']['fields']['fuzzy'] = array(
        'type' => 'int', 
        'size' => 'tiny', 
        'not null' => TRUE, 
        'not null' => FALSE, 
        'disp-width' => '4',
        'description' => t('Indicates that the translation has not yet been verified. Automatic translations are fuzzy by default.')
    );

    return $schema;
}